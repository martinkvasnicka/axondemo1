package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId
import java.time.LocalDate

data class CreateMemberCommand(
    override val memberId: MemberAggregateId,
    val firstName: String,
    val membershipEndDate: LocalDate
) : MemberCommand(memberId)
