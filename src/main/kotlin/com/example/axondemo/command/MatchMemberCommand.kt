package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId

data class MatchMemberCommand(
    override val memberId: MemberAggregateId,
    val doCreate: Boolean
) : MemberCommand(memberId)
