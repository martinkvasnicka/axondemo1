package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId
import java.time.LocalDate

data class UpdateMemberCommand(
    override val memberId: MemberAggregateId,
    val firstName: String,
    val membershipEndDate: LocalDate
) : MemberCommand(memberId)
