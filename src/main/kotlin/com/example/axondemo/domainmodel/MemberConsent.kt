package com.example.axondemo.domainmodel

import java.time.LocalDate

data class MemberConsent(
    val consentCode: String,
    var validFrom: LocalDate?,
    var validTo: LocalDate?
) {
    fun isTransferable(): Boolean {
        return (validFrom == null || !validTo!!.isAfter(LocalDate.now())) &&
            (validTo == null || validTo!!.isAfter(LocalDate.now()))
    }
}
