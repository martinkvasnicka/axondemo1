package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId
import org.axonframework.modelling.command.TargetAggregateIdentifier

abstract class MemberCommand(
    @TargetAggregateIdentifier
    open val memberId: MemberAggregateId
)
