package com.example.axondemo.domainmodel

data class MemberAggregateId(
    val organizationCode: String,
    val externalId: String,
    val companyId: String? = null
)
