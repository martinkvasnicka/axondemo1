package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId

data class MemberDeletedEvent(
    override val memberId: MemberAggregateId
) : ParentMemberEvent(memberId)
