package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId
import java.time.LocalDate

data class ConsentUpdatedEvent(
    override val memberId: MemberAggregateId,
    val consentCode: String,
    val validFrom: LocalDate?,
    val validTo: LocalDate?
) : ParentMemberEvent(memberId)
