package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId
import com.example.axondemo.model.MemberData
import java.time.LocalDate
import java.util.UUID

data class MemberCreatedEvent(
    override val memberId: MemberAggregateId,
    val entityId: UUID,
    private val firstName: String,
    private val membershipEndDate: LocalDate
) : ParentMemberEvent(memberId), MemberData {
    override fun getFirstName() = firstName

    override fun getMembershipEndDate() = membershipEndDate
}
