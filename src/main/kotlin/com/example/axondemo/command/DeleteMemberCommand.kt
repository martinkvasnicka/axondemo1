package com.example.axondemo.command

import com.example.axondemo.domainmodel.MemberAggregateId

data class DeleteMemberCommand(
    override val memberId: MemberAggregateId
) : MemberCommand(memberId)
