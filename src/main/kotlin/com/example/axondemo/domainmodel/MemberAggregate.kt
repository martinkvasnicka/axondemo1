package com.example.axondemo.domainmodel

import com.example.axondemo.command.AssignMarkrefCommand
import com.example.axondemo.command.AssignPartrefCommand
import com.example.axondemo.command.CreateConsentCommand
import com.example.axondemo.command.CreateMemberCommand
import com.example.axondemo.command.DeleteMemberCommand
import com.example.axondemo.command.UpdateConsentCommand
import com.example.axondemo.command.UpdateMemberCommand
import com.example.axondemo.command.UpdateMembershipEndDateCommand
import com.example.axondemo.event.ConsentCreatedEvent
import com.example.axondemo.event.ConsentUpdatedEvent
import com.example.axondemo.event.FirstTransferableConsentAddedEvent
import com.example.axondemo.event.LastTransferableConsentRemovedEvent
import com.example.axondemo.event.MarkrefAssignedEvent
import com.example.axondemo.event.MemberCreatedEvent
import com.example.axondemo.event.MemberDeletedEvent
import com.example.axondemo.event.MemberMatchedEvent
import com.example.axondemo.event.MemberUpdatedEvent
import com.example.axondemo.event.MembershipEndDateUpdatedEvent
import com.example.axondemo.model.MemberData
import mu.KotlinLogging
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate
import java.time.LocalDate
import java.util.Objects
import java.util.UUID

@Aggregate(snapshotTriggerDefinition = "snapshotTriggerDefinition") // , cache = "aggregateCache")
@ProcessingGroup("memberGroup")
data class MemberAggregate(
    private var entityId: UUID?,
    private var firstName: String? = null,
    private var membershipEndDate: LocalDate? = null,
    private var partref: String? = null,
    private val consents: MutableList<MemberConsent> = mutableListOf()
) {

    private val logger = KotlinLogging.logger {}

    @Id
    @AggregateIdentifier
    private lateinit var memberId: MemberAggregateId

    // for Axon purposes, if it re-creates aggregate from the events
    protected constructor() : this(null)

    @CommandHandler
    constructor(cmd: CreateMemberCommand) : this() {
        logger.info("CreateMemberCommand")
        AggregateLifecycle.apply(MemberCreatedEvent(cmd.memberId, UUID.randomUUID(), cmd.firstName, cmd.membershipEndDate))
    }

    @CommandHandler
    fun handle(cmd: UpdateMemberCommand) {
        logger.info("UpdateMemberCommand")
        if (!Objects.equals(this.membershipEndDate, cmd.membershipEndDate)) {
            AggregateLifecycle.apply(MembershipEndDateUpdatedEvent(cmd.memberId, cmd.membershipEndDate))
        }
        AggregateLifecycle.apply(MemberUpdatedEvent(cmd.memberId, cmd.firstName, cmd.membershipEndDate))
    }

    @CommandHandler
    fun handle(cmd: UpdateMembershipEndDateCommand) {
        logger.info("UpdateMembershipEndDateCommand")
        AggregateLifecycle.apply(MembershipEndDateUpdatedEvent(cmd.memberId, cmd.date))
    }

    @CommandHandler
    fun handle(cmd: CreateConsentCommand, commandGateway: CommandGateway) {
        logger.info("CreateConsentCommand ${cmd.consentCode}")
        if (consentExists(cmd.consentCode)) {
            throw IllegalStateException("consent ${cmd.consentCode} exists already")
        }
        AggregateLifecycle.apply(ConsentCreatedEvent(cmd.memberId, cmd.consentCode, cmd.validFrom, cmd.validTo))
            .andThen {
                if (isFirstTransferableConsent(cmd.consentCode)) {
                    AggregateLifecycle.apply(FirstTransferableConsentAddedEvent(this.memberId))
                }
            }
    }

    @CommandHandler
    fun handle(cmd: UpdateConsentCommand) {
        logger.info("UpdateConsentCommand ${cmd.consentCode}")
        if (!consentExists(cmd.consentCode)) {
            throw IllegalStateException("consent ${cmd.consentCode} does not exist")
        }
        AggregateLifecycle.apply(ConsentUpdatedEvent(cmd.memberId, cmd.consentCode, cmd.validFrom, cmd.validTo)).andThen {
            if (isFirstTransferableConsent(cmd.consentCode)) {
                AggregateLifecycle.apply(FirstTransferableConsentAddedEvent(this.memberId))
            } else if (hasTransferableConsents()) {
                AggregateLifecycle.apply(LastTransferableConsentRemovedEvent(this.memberId))
            }
        }
    }

    @CommandHandler
    fun handle(cmd: DeleteMemberCommand) {
        logger.info("DeleteMemberCommand")
        AggregateLifecycle.apply(MemberDeletedEvent(cmd.memberId))
    }

    @CommandHandler
    fun handle(cmd: AssignPartrefCommand) {
        logger.info("AssignPartrefCommand")
        AggregateLifecycle.apply(MemberMatchedEvent(cmd.memberId, cmd.partref))
    }

    @CommandHandler
    fun handle(cmd: AssignMarkrefCommand) {
        logger.info("AssignMarkrefCommand")
        AggregateLifecycle.apply(MarkrefAssignedEvent(cmd.memberId, cmd.partref, cmd.markref))
    }

    @EventSourcingHandler
    fun on(event: MemberCreatedEvent) {
        logger.info("MemberCreatedEvent")
        this.memberId = event.memberId
        this.entityId = event.entityId
        copyFromModel(event)
    }

    @EventSourcingHandler
    fun on(event: MemberUpdatedEvent) {
        logger.info("MemberUpdatedEvent")
        copyFromModel(event)
    }

    @EventSourcingHandler
    fun on(event: MembershipEndDateUpdatedEvent) {
        logger.info("MembershipEndDateUpdatedEvent")
        this.membershipEndDate = event.date
        // update co-members...
    }

    @EventSourcingHandler
    fun on(event: ConsentCreatedEvent) {
        logger.info("ConsentCreatedEvent")
        val memberConsent = MemberConsent(event.consentCode, event.validFrom, event.validTo)
        this.consents.add(memberConsent)
    }

    @EventSourcingHandler
    fun on(event: ConsentUpdatedEvent) {
        logger.info("ConsentUpdatedEvent")
        getConsent(event.consentCode)?.let {
            it.validFrom = event.validFrom
            it.validTo = event.validTo
        }
    }

    @EventSourcingHandler
    fun on(event: MemberMatchedEvent, commandGateway: CommandGateway) {
        logger.info("MemberMatchedEvent")
        this.partref = event.partref
    }

    @EventSourcingHandler
    fun on(event: LastTransferableConsentRemovedEvent) {
        logger.info("LastTransferableConsentRemovedEvent")
    }

    @EventSourcingHandler
    fun on(event: MemberDeletedEvent) {
        logger.info("MemberDeletedEvent")
        AggregateLifecycle.markDeleted()
    }

    private fun getConsent(consentCode: String) =
        consents.firstOrNull { it.consentCode == consentCode }

    private fun consentExists(consentCode: String) =
        consents.any { it.consentCode == consentCode }

    private fun hasTransferableConsents() =
        consents.any { it.isTransferable() }

    private fun isFirstTransferableConsent(consentCode: String): Boolean {
        val firstTransferableConsent = this.consents.filter { it.isTransferable() }.minByOrNull { it.consentCode }!!
        return firstTransferableConsent.consentCode == consentCode
    }

    // some mapper in real life
    private fun copyFromModel(member: MemberData) {
        this.firstName = member.getFirstName()
        this.membershipEndDate = member.getMembershipEndDate()
    }
}
