package com.example.axondemo.event

import com.example.axondemo.domainmodel.MemberAggregateId

data class FirstTransferableConsentAddedEvent(override val memberId: MemberAggregateId) :
    ParentMemberEvent(memberId)
